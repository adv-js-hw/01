class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    // console.log(`The name of employee is ${this._name}`);
    return this._name;
  }

  get age() {
    // console.log(`The age is: ${this._age} years`);
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(newName) {
    newName = newName.trim();
    if (newName === "") {
      throw "The name cannot be empty";
    }
    this._name = newName;
  }

  set age(newAge) {
    if (newAge < 18) {
      throw Error("Child labor is prohibited by law");
    } else if (newAge > 80) {
      throw Error("Too old");
    } else this._age = newAge;
  }

  set salary(newSalary) {
    if (newSalary < 3000 && newSalary > 200000) {
      throw Error("Unacceptable salary level");
    }

    this._salary = newSalary;
  }

  programCard() {
    let fullname = this._name.split(" ");
    let firstName = fullname[0];
    let lastName = fullname[1];

    return `Card for employee is ${this._name}:
    First Name: ${firstName}
    Last Name: ${lastName}
    Age: ${this.age} years
    Initiated Salary: $${this._salary}
    Programmer Salary: $${this.salary}
    Language: ${this.lang}\n
    Comments: ${this.comments()}
    `;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this.programmerSalary();
  }

  get lang() {
    // console.log(`Languages: ${this._lang}`);
    return this._lang;
  }

  programmerSalary() {
    return this._salary * 3;
  }

  comments() {
    return `The initiated salary of $${this._salary} for $${
      this._name
    } is multiplied by 3 and now equals $${this.programmerSalary()}`;
  }
}

const billGates = new Programmer("Bill Gates", 68, 150000, "English");
const zuckerberg = new Programmer("Mark Zuckerberg", 39, 86000, "Spanish");
const torvalds = new Programmer("Linus Torvalds", 54, 49000, "Portugues");

console.log(billGates);
console.log(zuckerberg);
console.log(torvalds);

// console.log(`Salary of $${billGates.salary} is acceptable`);

console.log(billGates.programCard());
console.log(zuckerberg.programCard());
console.log(torvalds.programCard());
